// - Zmienne globalne - //

var word, wordLength, hiddenWord, misguided, yes, no, i;

word = "Bez pracy nie ma kołaczy";
word = word.toUpperCase();
wordLength = word.length;
hiddenWord = "";
misguided = 0;

yes = new Audio("yes.wav");
no = new Audio("no.wav");

// - Maskuje hasło do odgadnięcia i wypisuje je na stronie - //

for (i = 0; i < wordLength; i += 1) {
    
    if (word.charAt(i) === " ") {
        hiddenWord = hiddenWord + " ";
    } else {
        hiddenWord = hiddenWord + "-";
    }
    
}

function writeWord() {
    "use strict";
    document.getElementById("board").innerHTML = hiddenWord;
}

// - Tablica przechowująca litery alfabetu - //

var letters = new Array(35);
letters[0] = "A";
letters[1] = "Ą";
letters[2] = "B";
letters[3] = "C";
letters[4] = "Ć";
letters[5] = "D";
letters[6] = "E";
letters[7] = "Ę";
letters[8] = "F";
letters[9] = "G";
letters[10] = "H";
letters[11] = "I";
letters[12] = "J";
letters[13] = "K";
letters[14] = "L";
letters[15] = "Ł";
letters[16] = "M";
letters[17] = "N";
letters[18] = "Ń";
letters[19] = "O";
letters[20] = "Ó";
letters[21] = "P";
letters[22] = "Q";
letters[23] = "R";
letters[24] = "S";
letters[25] = "Ś";
letters[26] = "T";
letters[27] = "U";
letters[28] = "V";
letters[29] = "W";
letters[30] = "X";
letters[31] = "Y";
letters[32] = "Z";
letters[33] = "Ż";
letters[34] = "Ź";

// - Po załadowaniu strony generuje divy z literami alfabetu - //

function start() {
    
    "use strict";
    var content;
    content = "";
    
    for (i = 0; i <= 34; i += 1) {
        
        var el = "letter" + i;
        content = content + '<div onclick="checkLetterState(' + i + ')" class="letter" id="' + el + '">' + letters[i] + '</div>';
        
        if ((i + 1) % 7 === 0) {
            content = content + '<div style="clear:both;">';
        }
        
    }
    
    document.getElementById("alphabet").innerHTML = content;
    
    writeWord();
    
}
window.onload = start;

// - Podmiana znaku - //

String.prototype.swapTheDash = function (place, char) {
    
    // dla pewnosci, ze pozycja znaku nie bedzie wieksza niz dlugosc ciagu
    if (place > this.length - 1) {
        return this.toString();
    } else {
        return this.substr(0, place) + char + this.substr(place + 1);
    }
    
};

// - Sprawdzenie stanu litery: kliknięta -> zmiana koloru na zielony lub czerwony - //

function checkLetterState(divNumber) {
    
    "use strict";
    var hitLetter;
    hitLetter = false;
    
    for(i = 0; i < wordLength; i += 1) {
        if (word.charAt(i) === letters[divNumber]) {
            hiddenWord = hiddenWord.swapTheDash(i, letters[divNumber]);
            hitLetter = true;
        }  
    }
    
    if (hitLetter === true) {
        yes.play();
        var el = "letter" + divNumber;
        document.getElementById(el).style.background = "#003300";
        document.getElementById(el).style.color = "#00C000";
        document.getElementById(el).style.border = "3px solid #00C000";
        document.getElementById(el).style.cursor = "default";
        writeWord();
    } else {
        no.play();
        var el = "letter" + divNumber;
        document.getElementById(el).style.background = "#330000";
        document.getElementById(el).style.color = "#C00000";
        document.getElementById(el).style.border = "3px solid #C00000";
        document.getElementById(el).style.cursor = "default";
        document.getElementById(el).setAttribute("onclick", ";");
        misguided++;
        var img = "img/s" + misguided + ".jpg";
        document.getElementById("hangman").innerHTML = '<img src="' + img + '" alt="" />';
    }
    
    // - Wygrana - //
    
    if (word === hiddenWord) {
        document.getElementById("alphabet").innerHTML = "<br/><br/>Gratulacje! Prawidłowe hasło: " + word + '<br/><br/><span class="reset" onclick="location.reload()">JESZCZE RAZ?</span>';
    }
    
    // - Przegrana - //
    
    if (misguided >= 9) {
        document.getElementById("alphabet").innerHTML = "<br/><br/>Przegrana! Prawidłowe hasło: " + word + '<br/><br/><span class="reset" onclick="location.reload()">JESZCZE RAZ?</span>';
    }
}




















